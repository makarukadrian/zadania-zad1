import unittest
from sum_client import client
import socket



class SumTestCase(unittest.TestCase):

    def test_sum(self):
        message = '13 13'
        actual = client(message)
        expected = '26.0'
        self.assertEqual(expected, actual)

    def test_sum_error1(self):
        msg = 'qwe 123'
        actual = client(msg)
        expected = "Niepoprawne dane"
        self.assertEqual(expected, actual)

    def test_sum_error2(self):
        msg = '123 qwe'
        actual = client(msg)
        expected = "Niepoprawne dane"
        self.assertEqual(expected, actual)

    def test_sum_error3(self):
        msg = 'qwe qwe'
        actual = client(msg)
        expected = "Niepoprawne dane"
        self.assertEqual(expected, actual)


if __name__ == '__main__':
    unittest.main()