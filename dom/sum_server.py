# -*- coding: utf-8 -*-
import socket
import sys
import logging
import logging.config


def server():

    server_address = ('194.29.175.240', 1213)
    # Tworzenie gniazda TCP/IP
    s = socket.socket()
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    # Powiązanie gniazda z adresem
    s.bind(server_address)
    # Nasłuchiwanie przychodzących połączeń
    s.listen(1)
    try:
        # Nieskończona pętla pozwalająca obsługiwać dowolną liczbę połączeń
        # ale tylko jedno na raz
        while True:
            # Czekanie na połączenie
            connection, address = s.accept()

            try:
                # Odebranie danych
                buffsize = 4096
                done = False
                data = ''
                while not done:
                    part = connection.recv(buffsize)
                    if len(part) < buffsize:
                        done = True
                        data += part


                # Odesłanie odebranych danych spowrotem
                data = data.split(" ")
                suma = float(data[0]) + float(data[1])
                connection.sendall(str(suma))
            except:
                connection.sendall("Error")

            finally:
                # Zamknięcie połączenia
                connection.close()


    except KeyboardInterrupt:

        s.close()

if __name__ == '__main__':
    server()
    sys.exit(0)