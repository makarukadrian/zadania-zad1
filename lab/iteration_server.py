# -*- coding: utf-8 -*-

import socket
import sys
import logging
import logging.config


def server(logger):
    """ Serwer iteracyjny zwracajacy kolejny numer połączenia
    logger - mechanizm do logowania wiadomości
    """
    server_address = ('localhost', 4444)  # TODO: zmienić port!

    # Ustawienie licznika na zero
    count = 0

    # Tworzenie gniazda TCP/IP

    # Powiązanie gniazda z adresem

    # Nasłuchiwanie przychodzących połączeń
    logger.info(u'tworzę serwer na {0}:{1}'.format(*server_address))

    try:
        while True:
            # Czekanie na połączenie
            logger.info(u'czekam na połączenie')

            # Nawiązanie połączenia
            addr = ('', '')
            logger.info(u'połączono z {0}:{1}'.format(*addr))

            # Podbicie licznika

            try:
                # Wysłanie wartości licznika do klienta

                logger.info(u'wysłano {0}'.format(count))

            finally:
                # Zamknięcie połączenia

                logger.info(u'zamknięto połączenie')

    except KeyboardInterrupt:
        pass


if __name__ == '__main__':
    logging.config.fileConfig('logging.conf')
    logger = logging.getLogger('iteration_server')
    server(logger)
    sys.exit(0)