# -*- coding: utf-8 -*-

import socket
import sys
import logging
import logging.config


def server(logger):
    """ Serwer echo zwracający otrzymane dane
    logger - mechanizm do logowania wiadomości
    """
    server_address = ('localhost', 4444)  # TODO: zmienić port!

    # Tworzenie gniazda TCP/IP
    # TODO: wstawić kod tworzący nowe gniazdo sieciowe
    # TODO: ustawić opcję pozwalającą na natychmiastowe ponowne użycie gniazda
    #       (zobacz koniec http://docs.python.org/2/library/socket.html)

    # Powiązanie gniazda z adresem
    # TODO: powiązać gniazdo z adresem

    # Nasłuchiwanie przychodzących połączeń
    logger.info(u'tworzę serwer na {0}:{1}'.format(*server_address))
    # TODO: uaktywnić nasłuchiwanie na przychodzące połąćzenia

    try:
        # Nieskończona pętla pozwalająca obsługiwać dowolną liczbę połączeń
        # ale tylko jedno na raz
        while True:
            # Czekanie na połączenie
            logger.info(u'czekam na połączenie')
            # TODO: stwórz nowe gniazdo dla przychodzącego połączenia
            #       adres klienta umieść w zmiennej addr
            addr = ('', '')

            logger.info(u'połączono z {0}:{1}'.format(*addr))

            try:
                # Odebranie danych
                # TODO: odbierz dane od klienta i umieść je w zmiennej data
                data = ''

                logger.info(u'otrzymano "{0}"'.format(data))

                # Odesłanie odebranych danych spowrotem
                # TODO: odeślij klientowi te same dane, które wcześniej przesłał

                logger.info(u'odesłano wiadomość do klienta')

            finally:
                # Zamknięcie połączenia
                # TODO: zamknij połączenie sieciowe

                logger.info(u'zamknięto połączenie')

    except KeyboardInterrupt:
        # TODO: użyj wyjątku KeyboardIntterupt jako sygnału do zamknięcia gniazda
        #       i zakończenia działania serwera
        #       zastąp słowo pass odpowiednim kodem
        pass


if __name__ == '__main__':
    logging.config.fileConfig('logging.conf')
    logger = logging.getLogger('echo_server')
    server(logger)
    sys.exit(0)